@echo off
setlocal enabledelayedexpansion
ECHO Script for batch transform XYZ to another CS
ECHO Version: 1.2
ECHO Author: Slawomir Figiel, edit Marek Szczepkowski
ECHO Date: 22-08-2019
ECHO Company: Visimind Ltd

REM ----- USER CAN EDIT HERE ----------
REM In EPSG code or proj4 format
SET INPUT_CRS=EPSG:2180
SET OUTPUT_CRS=EPSG:2177

REM ---IF HEADER HAS TAB REPLACE SET HEADER=X Y Z by SET HEADER=X	Y	Z
SET HEADER=X Y Z 
SET PDAL_BIN_PATH=C:\Programy\PDAL\bin
SET PYTHON_EXE=C:\Program Files\QGIS 2.18\bin\python.exe






REM ----- DONT EDIT BELOW ----------------
SET WORK=%cd%
call "%PDAL_BIN_PATH%\o4w_env.bat"

REM CREATE OUTPUT FOLDER IF DON'T EXISTS
if not exist "%WORK%\Transformed" mkdir "%WORK%\Transformed"

REM COUNTER FILES
dir /b *.xyz 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1
ECHO.

FOR /F %%i IN ('dir /b "%WORK%\*.xyz"') DO (
    echo %%i   !Counter! / %count% FILES

	pdal translate -r readers.text "%%i" "%%~ni_temp.xyz" reprojection --filters.reprojection.in_srs="%INPUT_CRS%" --filters.reprojection.out_srs="%OUTPUT_CRS%" --readers.text.header="%HEADER%" --writers.text.delimiter="	"
	"%PYTHON_EXE%" remove_first_line.py "%%~ni_temp.xyz" "%WORK%\Transformed\%%~ni_2000s6.xyz"

	DEL "%%~ni_temp.xyz"
	set /A Counter+=1
	ECHO.

)
PAUSE