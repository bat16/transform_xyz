from datetime import datetime
import os
import sys

temporary_filename = 'temp.tmp'

if len(sys.argv) < 2:
    print('Provide input path')
    sys.exit(1)

if len(sys.argv) < 3:
    print('Provide output path')
    sys.exit(1)

fileName = sys.argv[1]
output_filename = sys.argv[2]

is_first = True

if os.path.exists(output_filename):
    os.remove(output_filename)

with open(fileName, 'rt') as in_file, open(temporary_filename, 'wt+') as out_file:
    for line in in_file.readlines():
        if is_first:
            is_first = False
            continue
        out_file.write(line)

os.rename(temporary_filename, output_filename)
